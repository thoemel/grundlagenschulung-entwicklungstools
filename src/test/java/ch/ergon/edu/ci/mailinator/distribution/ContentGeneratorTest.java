package ch.ergon.edu.ci.mailinator.distribution;

import ch.ergon.edu.ci.mailinator.domain.Letter;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class ContentGeneratorTest {

    private static final int CONTENT_LEN = 128;
    private final ContentGenerator contentGenerator = new ContentGenerator();

    /**
     * Unit Test zu Aufgabe 2
     */
    @Ignore
    @Test
    public void letterMustContainContent() {
        Letter letter = new Letter();
        contentGenerator.insertContent(letter);
        Assert.assertThat(letter.getContent(), Matchers.not(Matchers.isEmptyOrNullString()));
    }

    /**
     * Unit Test zu Aufgabe 2
     */
    @Ignore
    @Test
    public void contentMustHaveCorrectLength() {
        Letter letter = new Letter();
        contentGenerator.insertContent(letter);
        Assert.assertThat(letter.getContent().length(), Matchers.is(CONTENT_LEN));
    }

}
